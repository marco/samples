//
// Created by Marco Massenzio on 11/5/24.
//
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string>

#include "queue.h"

using namespace std;
using namespace testing;

// TODO: add an example how to use GMock
class QueueTest : public Test {
protected:
    void SetUp() override {
      cout << "Setting up QueueTest" << endl;
    }
};

TEST(QueueTest, DefaultConstructor) {
  ThreadsafeQueue<int> testQ;
  EXPECT_EQ(testQ.size(), 0);
  EXPECT_EQ(testQ.pop(), nullopt);
}

TEST(QueueTest, MoveConstructor) {
  ThreadsafeQueue<int> testQ;
  testQ.push(42);
  testQ.push(24);
  EXPECT_EQ(testQ.size(), 2);

  auto moved{std::move(testQ)};
  ASSERT_EQ(moved.size(), 2);
  ASSERT_EQ(testQ.size(), 0);

  EXPECT_EQ(moved.pop(), 42);
  EXPECT_EQ(moved.pop(), 24);
}

TEST(QueueTest, PushPop) {
  ThreadsafeQueue<int> testQ;
  testQ.push(42);
  testQ.push(24);
  EXPECT_EQ(testQ.size(), 2);

  EXPECT_EQ(testQ.pop(), 42);
  EXPECT_EQ(testQ.size(), 1);
  EXPECT_EQ(testQ.pop(), 24);
  EXPECT_EQ(testQ.size(), 0);
}

TEST(QueueTest, Destructor) {
  // Attempting to destroy a non-empty queue should throw an exception
  ASSERT_THROW(
      {
        ThreadsafeQueue<int> testQ;
        testQ.push(42);
        testQ.push(24);
      }, non_empty_queue);

  // Destroying an empty queue should not throw
  ThreadsafeQueue<int> testQ;
  testQ.push(42);
  testQ.push(24);
  testQ.clear();
}


TEST(QueueTest, SpaceshipOp) {
  ThreadsafeQueue<string> q1, q2;
  q1.push("Pink Floyd");
  q1.push("Abba");
  q2.push("Pink Floyd");
  q2.push("Marillion");

  EXPECT_TRUE(q1 < q2);
  q2.pop();
  // Now "Marillion" is at head of q2 and < "Pink Floyd"
  EXPECT_TRUE(q2 < q1);
  // Clean up to avoid exceptions when destroying.
  q1.clear();
  q2.clear();
}

TEST(QueueTest, SpaceshipOpWithMutex) {
  GTEST_SKIP() << "SpaceshipOpWithMutex is disabled because std::mutex is not three-way comparable.";
  ThreadsafeQueue<std::mutex> q1, q2;
  // Example of a type that cannot be compared; the
  // above line compiles (as the operator is not invoked)
  // but the following line will not compile.

  //  EXPECT_TRUE(q1 < q2);
}

TEST(QueueTest, TestPeek) {
  ThreadsafeQueue<string> q;
  q.push("safe");
  ASSERT_TRUE(q.peek());
  EXPECT_EQ(*q.peek(), "safe");
  EXPECT_EQ(q.size(), 1);

  q.pop();
  ASSERT_FALSE(q.peek());
}
