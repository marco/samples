// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 07/16/20.

#include <iostream>
#include <iomanip>
#include <set>
#include <unordered_set>

#include "json.hpp"

using json = nlohmann::json;
using namespace std;

struct Bucket {
  int id;
  string s;
  std::map<string, float> values;
};

void to_json(json &j, const Bucket &b) {
  j = json{{"id", b.id}, {"name", b.s}, {"attrs", b.values}};
}

bool operator<(const Bucket &lhs, const Bucket &rhs) {
  return lhs.id < rhs.id;
}

int main(int argc, char **argv) {

  std::set<Bucket> bs {
      {33, "my values", {{"one", 1}, {"two", 2}}},
      {22, "my values", {{"tre", 1}, {"due", 2}}},
      {1, "my values", {{"dis", 1}, {"dat", 2}}}
  };

  json data;
  data["bucket"] = json { bs };

  cout << std::setw(4) << data << endl;

  return EXIT_SUCCESS;
};
