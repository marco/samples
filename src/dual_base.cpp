//
// Created by Marco Massenzio on 8/26/24.
//

#include <iostream>

using namespace std;


class Processor {
public:
    virtual void process() = 0;

    virtual ~Processor() {
        cout << "Processor destroyed" << endl;
    }
};

class StringProcessor : public Processor {
public:
    void process() override {
        cout << "Processing a string..." << endl;
    }
};

class IntegerProcessor : public Processor {
public:
    void process() override {
        cout << "Processing an integer..." << endl;
    }
};

class MyProcessor : public StringProcessor, IntegerProcessor {
public:
    explicit MyProcessor(int val) : value(val) {
        cout << "MyProcessor created" << endl;
    }
    ~MyProcessor() override {
        cout << "MyProcessor "<< value << " destroyed" << endl;
    }

    void process() override {
        cout << "Processing a string with value " << value << "..." << endl;
    }

private:
    int value;
};

void doSomething(const std::shared_ptr<StringProcessor>& processor) {
    cout << "Doing something with a StringProcessor..." << endl;
    processor->process();
    cout << "Done" << endl;
}


int main() {
    cout << "Using MyProcessor..." << endl;
    StringProcessor processor;
    processor.process();

    cout << "Using MyProcessor..." << endl;
    doSomething(std::shared_ptr<StringProcessor>(&processor, [](StringProcessor *p) {
        cout << "Doing nothing" << endl;
    }));

    cout << "Using MyProcessor after having done something..." << endl;
    processor.process();

    return 0;
}
