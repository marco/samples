// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 9/2/17.

#include <iostream>
#include <utility>
#include <vector>
#include <sstream>
#include <thread>
#include <type_traits>


using std::cout;
using std::endl;
using std::string;

// Handling misses in lookups.
class not_found : public std::exception {

  std::string what_;

public:
  explicit not_found(string what) : what_(std::move(what)) {}

  [[nodiscard]] const char* what() const noexcept override {
    return what_.c_str();
  }
};

template <typename T>
class LoggingPolicy {
public:
  virtual void log(const T& val) const = 0;
};

template <typename T>
class Quiet : public LoggingPolicy<T> {
public:
  void log(const T& val) const override { }
};

template <typename T>
class Verbose : public LoggingPolicy<T> {
public:
  void log(const T& val) const override {
    std::cout << val << std::endl;
  }
};


template <typename Key, typename Value>
class Pair {
  Key key_;
  Value value_;

public:
  Pair(Key key, Value value) : key_(std::move(key)), value_(std::move(value)) { }

  Key key() const { return key_; }
  Value value() const { return value_; }

  bool operator==(const Pair& other) {
    return other.key_ == key_ && other.value_ == value_;
  }

  bool operator<(const Pair& other) {
    return other.key_ < key_;
  }
};

template <typename Key, typename Value>
std::ostream& operator<<(std::ostream& out, const Pair<Key, Value>& pair)
{
  out << pair.key() << ": " << pair.value();
  return out;
}


template <typename Key, typename Value,
    template <typename> class Logging = Quiet>
class Properties : public Logging<Pair<Key, Value>> {
  std::vector<Pair<Key, Value>> properties_;

public:
  void add(const Key& key, const Value& value);
  Value get(const Key& key) const;
  Value getOrElse(const Key& key, const Value& other) const;

  void PrintAll(std::ostream& out = std::cout) const {
    for (const auto &entry : properties_) {
      out << entry << endl;
    }
  }
};


template <typename Key, typename Value,
    template <typename> class Logging>
void Properties<Key, Value, Logging>::add(const Key &key, const Value &value) {
  Pair<Key, Value> entry(key, value);
  this->log(entry);
  properties_.push_back(entry);
}

template <typename Key, typename Value,
    template <typename> class Logging>
Value Properties<Key, Value, Logging>::get(const Key &key) const {
  for (const auto &entry : properties_) {
    if (entry.key() == key) {
      this->log(Pair<Key, Value>(entry));
      return entry.value();
    }
  }
  std::ostringstream ostrm;
  ostrm << key;
  throw not_found(ostrm.str());
}

template <typename Key, typename Value,
    template <typename> class Logging>
Value Properties<Key, Value, Logging>::getOrElse(const Key &key, const Value &other) const {
  try {
    auto value = get(key);
    this->log(Pair<Key, Value>(key, value));
    return value;
  } catch (not_found& nf) {
    return other;
  }
}
template <typename Key, typename Value,
    template <typename> class Logging = Quiet,
    typename T>
void PrintProperties(const T& properties) {
  static_assert(std::is_base_of<Properties<Key, Value, Logging>, T>::value,
                "Must be a Properties class, or a derived one");

  properties.PrintAll();
}


template <typename Key, typename Value,
    template <typename> class Logging = Quiet>
class TitledProperties : public Properties<Key, Value, Logging> {
  // A properties class that can have a title.

  std::string title_;

public:
  explicit TitledProperties(string title) : title_(std::move(title)) {}

  // NOTE - this is NOT virtual
  void PrintAll(std::ostream& out = std::cout) const {
    out << "Title: " << title_ << std::endl;
    Properties<Key, Value, Logging>::PrintAll(out);
  }
};

template <typename T>
class MyFoo {
public:
  explicit MyFoo(const string& ss) {}
};


int main(int argc, char* argv[])
{
  Properties<string, string, Verbose> props;
  props.add("one", "uno");
  props.add("two", "due");
  props.add("three", "tre");

  cout << "---" << endl;

  cout << "Three: " << props.get("three") << endl;
  cout << "Four: " << props.getOrElse("four", "-- not found --") << endl;

  try {
    props.get("foo");
  } catch (const not_found &ex) {
    cout << ex.what() << endl;
  }

  // This is equivalent to: props.PrintAll();
  PrintProperties<string, string, Verbose>(props);

  TitledProperties<string, int> mapInts("String-to-Int Properties");
  mapInts.add("Nani", 7);
  mapInts.add("Porcellini", 3);
  mapInts.add("Biancaneve", 1);

  PrintProperties<string, int>(mapInts);

// This won't work, as the static_assert() fails
//  PrintProperties(MyFoo("This won't work!"));

  return EXIT_SUCCESS;
}
