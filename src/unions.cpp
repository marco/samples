// Copyright (c) 2016-2025 AlertAvert.com. All rights reserved.

#include <format>
#include <iostream>
#include <variant>

using namespace std;

namespace basics {
    namespace {
        void anon() {
            cout << "In anonymous namespace\n";
        }

    } // namespace

    class MyObject {
    public:
        explicit MyObject(int x) : x_(x) {
            cout << "Constructing MyObject with x=" << x_ << endl;
        }
        ~MyObject() { cout << std::format("Destroying MyObject({})", x_) << endl; }

        void show() const { cout << "MyObject: x=" << x_ << '\n'; }

    private:
        int x_;
    };

    class AnotherObject {
    public:
        explicit AnotherObject(double y) : y_(y) {
            cout << "Constructing AnotherObject with y=" << y_ << '\n';
        }

        ~AnotherObject() { cout << "Destroying AnotherObject\n"; }

        void show() const {
            anon();
            cout << "AnotherObject: y=" << y_ << '\n';
        }

    private:
        double y_;
    };

} // namespace basics


int main() {
//    using namespace basics;

    std::variant<basics::MyObject, basics::AnotherObject> v{basics::MyObject(42)};
    std::get<basics::MyObject>(v).show();

    v = basics::AnotherObject(3.14);
    std::get<basics::AnotherObject>(v).show();

    return 0;
}
