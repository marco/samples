// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 7/4/17.

#pragma once

#include <random>


template<typename T>
class Random {

  std::default_random_engine re_;
  std::normal_distribution<T> dist_;

public:
  Random(const T &mean, const T &stddev) : dist_{mean, stddev}, re_{} {}

  T operator()() {
    return dist_(re_);
  }
};
