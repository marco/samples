// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 3/20/16.

#include <future>

#include "distlib/utils/ThreadsafeQueue.hpp"

using PromisesQueue = ::utils::ThreadsafeQueue<
    std::shared_ptr<std::promise<double>>>;

void MeaningOfLife(PromisesQueue &queue) {
  bool done = false;
  while (!done) {
    auto p = queue.pop();
    if (p) {
      std::cout << "MOL setting value\n";
      (*p)->set_value(42.0);
      done = true;
    } else {
      std::cout << "MOL sleeping...\n";
      ::this_thread::sleep_for(10ms);
    }
  }
}

int main(int argc, char **argv) {
  PromisesQueue promises;

  std::thread t(MeaningOfLife, std::ref(promises));
  t.detach();

  auto answer = std::make_shared<std::promise<double>>();
  std::cout << "Queueing Promise\n";
  promises.push(answer);

  auto future = answer->get_future();
  std::cout << "Waiting to find the Meaning of Life...\n";
  future.wait();
  std::cout << "The meaning of life is " << future.get() << endl;

  return EXIT_SUCCESS;
}
