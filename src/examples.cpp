// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 3/20/16.

// Based on examples from Stroustroup's A Tour of C++ book.

#include <algorithm>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <list>
#include <regex>
#include <set>
#include <string>
#include <thread>

#include "random.h"

using namespace std;
using namespace std::chrono;


class error : public std::exception {
  std::string what_;
 public:
  explicit error(string what) noexcept: what_{std::move(what)} {}
  const char *what() const noexcept override { return what_.c_str(); }
};


template<typename T>
std::ostream &operator<<(std::ostream &stream, const std::vector<T> &vec) {
  for (const T &t : vec) {
    stream << t << " ";
  }
  return stream;
}


template<typename Iter>
void Print(const Iter &beg, const Iter &end) {
  for (auto pos = beg; pos != end; ++pos) {
    cout << *pos << endl;
  }
}


template<typename T>
void PrintVec(const vector<T> &vec, const string &sep = "\n") {
  bool first = true;

  for (auto t : vec) {
    if (!first) cout << sep; else first = false;
    cout << t;
  }
  cout << endl;
}


void partsum() {
  vector<int> v1{1, 2, 3, 4, 5, 6};
  vector<int> v2(v1.size());

  partial_sum(v1.begin(), v1.end(), v2.begin());
  PrintVec(v2);
}


void regex_demo() {
  string input = "192.168.51.123:8084";
  regex pat{R"((\d{1,3}(\.\d{1,3}){3}):(\d+))"};

  const string ip{"192.168.1.51"};
  const std::regex kIpPattern{R"(\d{1,3}(\.\d{1,3}){3})"};
  if (std::regex_match(ip, kIpPattern)) {
    cout << "Found a valid IP: " << ip << endl;
  }

  smatch matches;
  if (std::regex_match(input, matches, pat)) {
    cout << "As String: " << matches[1].str();
    cout << " and as an int: " << atoi(matches[3].str().c_str()) << endl;
    cout << "ip: " << matches[1] << ", port: " << matches[3] << "\n";
  }

  regex date_regex{R"((\d{4})-(\d{2})-(\d{2}))"};
  string date = "2017-Feb-04\n2016-12-First\n2016-04-13";
  if (regex_search(date, matches, date_regex)) {
    for (int i = 0; i < matches.size(); ++i) {
      if (matches[i].matched)
        cout << i << ": " << matches[i] << endl;
    }

  }
}


void print_floats() {
  cout.precision(8);
  cout << defaultfloat << 1234.56789 << ' '
       << fixed << 1234.56789 << ' '
       << 123456789 << '\n';

  istringstream values{"1.2 4.5 6.7"};

  float a, b, c;
  values >> a >> b >> c;
  cout.precision(2);
  cout << a << ", " << b << ", " << c << endl;

  std::vector<float> ints{1, 2, 3, 44};
  cout << ints << endl;
  auto copy = ints;
  copy.push_back(99);
  cout << "Copy: " << copy << endl;
  cout << "Original: " << ints << endl;
}


void use_pair() {
  auto p = make_pair(99, "problems");
  cout << p.first << ", " << p.second << endl;

  const auto&[num, val] = p;
  cout << num << " maps to " << val << endl;
}


void print_rnd(float mean = 1000, float stddev = 20) {
  Random<float> random(mean, stddev);
  array<int, 30> buckets{};

  double min = mean - 3 * stddev,
      max = mean + 3 * stddev,
      step = (max - min) / buckets.size();

  for (int &bucket : buckets) {
    bucket = 0;
  }

  for (int i = 0; i < 10000; ++i) {
    double x = random();
    if (x < min || x >= max)
      continue;
    int k = static_cast<int> (floor((x - min) / step));
    buckets[k]++;
  }

  double current = min;
  for (auto bucket : buckets) {
    std::cout << std::fixed << std::setprecision(2) << std::setw(8)
              << current << ' ' << std::string(bucket / 100, '*') << '\n';
    current += step;
  }
}


std::string trim(const std::string &str) {
  const auto strBegin = str.find_first_not_of(' ');

  if (strBegin == std::string::npos)
    return ""; // no content

  const auto strEnd = str.find_last_not_of(' ');
  const auto strRange = strEnd - strBegin + 1;

  return str.substr(strBegin, strRange);
}


std::vector<string> split(const string &values, const string &sep = ",",
                          bool trim_spaces = true, bool preserve_empty = true) {
  auto remains = values;
  std::vector<string> results;
  std::string::size_type pos;

  while ((pos = remains.find(sep)) != std::string::npos) {
    string value = remains.substr(0, pos);
    if (trim_spaces) {
      value = trim(value);
    }
    if (!value.empty() || preserve_empty) {
      results.push_back(value);
    }
    remains = remains.substr(pos + 1);
  }

  if (!remains.empty()) {
    results.push_back(remains);
  }
  return results;
}


void set_sort() {
  std::set<pair<std::string, int>> pairs;
  std::vector<pair<std::string, int>> vints;

  cout << "Size of vector: " << vints.size() << endl;
  pairs.insert(std::make_pair("Aldo", 33));
  pairs.insert(std::make_pair("Barrio", 41));
  pairs.insert(std::make_pair("Carlo", 15));
  pairs.insert(std::make_pair("Lisa", 4));
  pairs.insert(std::make_pair("Mario", 98));

  std::for_each(pairs.begin(), pairs.end(),
                [](pair<std::string, int> p) {
                  cout << p.first << ", " << p.second << endl;
                });
  for (const auto &pair : pairs) {
    vints.push_back(pair);
  }

  std::sort(vints.begin(), vints.end(), [](const pair<std::string, int> &t1,
                                           const pair<std::string, int> &t2) {
    return t1.second > t2.second;
  });
  for (auto p : vints) {
    cout << p.first << ": " << p.second << endl;
  }

}


struct Address {
  string street;
  string state;
  unsigned long zipcode;

  ~Address() {
    cout << "Destructor for " << street << " called" << endl;
  }

};

std::ostream& operator<<(std::ostream& out, const Address& a) {
  out << a.street << ", " << a.state << " " << a.zipcode << endl;
  return out;
}

class Holder {
 public:
  explicit Holder(const std::shared_ptr<Address>& other) : pint_{other} {
    cout << "Holder constructed for " << other->street << endl;
  }
  ~Holder() {
    cout << "Holder destructor" << endl;
  }
private:
  std::shared_ptr<Address> pint_;
};


void ptr_examples() {
  Address a {
    .street =  "Main St",
    .state =  "NY",
    .zipcode =  11001,
  };
  // Constructs a new object in memory.
  auto pa = std::make_unique<Address>("Pear Ave", "CA", 94087);
  cout << "(1) " << *pa << endl;
  // Invokes the copy constructor.
  auto pb = std::make_unique<Address>(a);
  cout << "(2) " << *pb << endl;

  pb->street = "Broadway & 3rd";
  // Here pb will call the destructor on its copy of the Address, but `a` remains valid.
  pb.reset();
  cout << "(3) " << a << endl;

  cout << "Before conversion to shared pa is " << (pa == nullptr ? "null" : "not null") << endl;
  std::shared_ptr<Address> spaddr = std::move(pa);
  cout << "After conversion pa is " << (pa == nullptr ? "null" : "not null") << endl;

  {
    cout << "Pass shared ptr to Holder" << endl;
    Holder keepAlive{ spaddr };
    cout << "Shared Ptr reset() - no destructor should be called" << endl;
    spaddr.reset();
    cout << "Holder goes out of scope here" << endl;
  }
  cout << "Method ends here" << endl;
}


struct Name {
  string name;

 public:
  void set_name(string n) {name = std::move(n);}
};

using NamePtr = std::shared_ptr<Name>;

bool operator<(const NamePtr & lhs, const NamePtr & rhs) {
  return lhs->name < rhs->name;
}

void sets() {
  // This set will have three elements, each with a different `name`,
  // thus respecting the contract of uniqueness of elements.
  std::set<NamePtr> names{
    std::make_shared<Name>(Name{"bob"}),
    std::make_shared<Name>(Name{"joe"}),
    std::make_shared<Name>(Name{"jack"}),
    std::make_shared<Name>(Name{"jack"}),
    std::make_shared<Name>(Name{"joe"})
  };
  for (auto &n : names) {
    cout << n->name << endl;
  }
  cout << "=====" << endl;

  // We violate the set contract, by changing the value of the element
  // "behind the back" of the set itself, which will have all elements
  // with the same name.
  auto pos = names.begin();
  auto n = (*pos)->name;
  while (++pos != names.end()) {
    // `pos` is a const_iterator, however const-ness is not transitive, and
    // will not transfer to the element pointed to by the const shared_ptr<>.
    // This allows us to do something "legal," but highly questionable.
    (*pos)->set_name(n);
  }
  // We end up with a set whose elements all end up having the same value.
  for (auto &pn : names) {
    cout << pn->name << endl;
  }
}

/**
 * Simple wrapper for a function call to add a separator, and time execution.
 *
 * @param name displayed to the user
 * @param func the function to call; takes no arguments, and returns none.
 */
void wrap(const std::string &name, std::function<void ()>func) {
  cout << "🛠️⚙️️" << name << " ⚙️🛠️\n";
  cout << "--------------------\n";
  auto start = system_clock::now();
  func();
  auto end = system_clock::now();
  cout << "--------------------\n";
  cout << "It took " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
       << " µsec\n";
}


int main(int argc, char *argv[]) {

  if (argc < 2) {
    std::filesystem::path exec_path(argv[0]);
    cerr << "Usage: " << exec_path.filename().string() << " <choice>" << endl;
    cerr << "Available options: regex_demo, print_floats, use_pair, partsum, print_rnd, set_sort, "
            "sets, ptr_examples" << endl;
    return EXIT_FAILURE;
  }
  string func_name = argv[1];

  if (func_name == "regex_demo") {
    wrap("RegEx Demo", regex_demo);
  } else if (func_name == "print_floats") {
    wrap("Floats Print", print_floats);
  } else if (func_name == "use_pair") {
    wrap("Pair", use_pair);
  } else if (func_name == "partsum") {
    wrap("Partial Sums", partsum);
  } else if (func_name == "print_rnd") {
    wrap("Random Numbers", []() { print_rnd(10, 3); });
  } else if (func_name == "set_sort") {
    wrap("Sort Set", set_sort);
  } else if (func_name == "sets") {
    wrap("Sets", sets);
  } else if (func_name == "ptr_examples") {
    wrap("Pointer Examples", ptr_examples);
  } else {
    cerr << "Unknown function name: " << func_name << endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
