// Created by marco on 7/19/20.

#include <chrono>
#include <cstdlib>
#include <iostream>

using namespace std::chrono;
using namespace std;


int main() {

  auto one_hour = 1h;
  auto anhour = 3'600s;
  auto msg = "They're one and the same"s;

  if (one_hour == anhour) {
    cout << msg << endl;
  } else {
    throw runtime_error("There are not enough seconds in an hour");
  }

  return EXIT_SUCCESS;
}
