#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <random>
#include <vector>

using namespace std;
using namespace std::placeholders;

template<typename F, typename S>
std::ostream &operator<<(std::ostream &out, const std::pair<F, S> item) {
  out << "{" << item.first << ", " << item.second << "}";
  return out;
}

template<typename C>
std::ostream &Print(
    const C &items,
    const std::string &coda = "\n",
    const char sep = ',',
    std::ostream &out = std::ref(std::cout)
) {
  out << '[';
  for (const auto &item : items) {
    out << item << sep;
  }
  out << '\b' << ']' << coda;
  return out;
}

/**
 * Fold Left operator
 *
 * @tparam C a container class
 * @tparam T the type of the contained type
 * @param init_value the initialization value
 * @param coll a `C<T>` container
 * @return the accumulation of all values (starting from the beginning, or "left" of the collection)
 */
template<typename C, typename T = typename C::value_type>
T operator/(const T &init_value, const C &coll) {
  return std::accumulate(coll.cbegin(), coll.cend(), init_value);
}

/**
 * Fold Right operator
 *
 * @tparam C a container class
 * @tparam T the type of the contained type
 * @param init_value the initialization value
 * @param coll a `C<T>` container
 * @return the accumulation of all values (starting from the end, or "right" of the collection)
 *      and reverse iterating
 */
template<typename C, typename T = typename C::value_type>
T operator%(const C &coll, const T &init_value) {
  return std::accumulate(coll.rbegin(), coll.rend(), init_value);
}

template<typename F, typename S>
inline std::pair<F, S> operator+(
    const std::pair<F, S> &lhs,
    const std::pair<F, S> &rhs) {
  return std::make_pair(lhs.first + rhs.first, lhs.second + rhs.second);
}

int main() {
  vector<int> ints{1, 12, 53, 84, 5, 88, 19, 10};
  map<string, int> ages{
      std::make_pair<string, int>("mario"s, 99),
      std::make_pair<string, int>("gigi"s, 34),
      std::make_pair<string, int>("beppe"s, 55),
  };

  cout << "FoldRight = " << std::vector<string>{"joe"s, "bill"s, "bob"s} % "reversed"s << endl;

  pair<const string, int> initial = std::make_pair<const string, int>("++"s, 10);
  cout << "FoldLeft = " << 101 / ints << endl;
  cout << "FoldRight = " << ints % -101 << endl;

  Print(ages) << endl;

  // Use partial function application, by binding args that are being reused
  // auto print_ints = std::bind(Print<vector<int>>, std::ref(ints), _1, ',', std::ref(std::cout));

  // Using Lambda syntax:
  auto print_l = [&ints](const std::string &coda) { Print(ints, coda); };

  sort(ints.begin(), ints.end());
  print_l(" sorted\n");

  auto pos = std::partition(ints.begin(), ints.end(),
                            [](int elem) { return elem % 2 == 0; });
  print_l(" after partition (even values)\n");
  cout << "Partition point: " << (pos - ints.begin()) << ", first element: " << *pos << endl;

  sort(ints.begin(), pos);
  print_l(" sorting up to partition point\n");

  std::random_device rd;
  std::mt19937 g(rd());
  std::shuffle(ints.begin(), ints.end(), g);
  print_l(" shuffled\n");

  stable_partition(ints.begin(), ints.end(),
                   [](int item) { return item < 50; });
  print_l(" after stable_partition (< 50)\n");

  return 0;
}
