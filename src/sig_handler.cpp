// Copyright (c) 2016-2025 AlertAvert.com. All rights reserved.

#include <csignal>
#include <fstream>
#include <iostream>

using namespace std;

namespace {
    volatile int _signal = 0;
}

auto PROMPT = " | "s;

/**
 * Signal handler for the program.
 *
 * @param sig the signal number
 */
void handler(int sig) {
  ::_signal = sig;
  cout << "\n\tHandler invoked for SIGNAL: " << sig << endl;
  cout << "\tPress ENTER to save data and exit..." << endl;
}

/**
 * Write the input text to the specified file.
 *
 * @param filename the name of the file to write to
 * @param text the text to write to the file
 */
void write(const string &filename, const string &text) {
  ofstream outfile(filename);
  if (outfile.is_open()) {
    outfile << text;
    outfile.close();
    cout << "Input saved to " << filename << ", exiting." << endl;
  } else {
    cerr << "Error opening file " << filename << endl;
  }
}

/**
 * Ask the user for a filename to save the input to.
 *
 * @return the filename entered by the user
 */
string askFilename() {
  string filename;
  cout << "Enter filename: ";
  std::getline(std::cin, filename);
  return filename;
}

/**
 * Initialize the signal handlers for the program.
 */
void initHandlers() {
  static const int SIGNALS[] = {SIGINT, SIGTERM, SIGABRT};
  for (auto sig : SIGNALS) {
    ::signal(sig, handler);
  }
}


int main(int argc, char **argv) {
  string buffer;
  string text;
  string filename;

  initHandlers();
  if (argc > 1) {
    filename = argv[1];
  } else {
    filename = askFilename();
  }

  cout << "\nSimple Editor -- Saving to: " << filename << endl;
  cout << "Press Ctrl-C to save & exit\n";

  int lineno = 1;
  while (::_signal == 0) {
    cout << lineno++ << PROMPT;
    std::getline(cin, buffer);
    text += buffer + '\n';
  }
  write(filename, text);

  return EXIT_SUCCESS;
}
