// Copyright (c) 2016-2025 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com) on 4/30/17.

#include <arpa/inet.h>
#include <cstdlib>
#include <cstdio>
#include <libgen.h>
#include <netdb.h>

void usage(const char *progname) {
  printf("Usage: %s HOST\n\nHOST\thostname to look up IP address for, required\n\n"
         "Finds the IP address for the given host\n", progname);
}

int main(int argc, char *argv[]) {
  struct addrinfo *_addrinfo;
  struct addrinfo *_res;
  char address[INET6_ADDRSTRLEN];
  int errcode = 0;

  if (argc < 2) {
    usage(basename(argv[0]));
    printf("ERROR: missing HOST argument\n");
    return EXIT_FAILURE;
  }
  auto host = argv[1];
  errcode = getaddrinfo(host, nullptr, nullptr, &_addrinfo);
  if (errcode != 0) {
    printf("ERROR: getaddrinfo: %s\n", gai_strerror(errcode));
    return EXIT_FAILURE;
  }

  for (_res = _addrinfo; _res != nullptr; _res = _res->ai_next) {
    if (_res->ai_family == AF_INET) {
      if (inet_ntop(AF_INET,
                    &((struct sockaddr_in *) _res->ai_addr)->sin_addr,
                    address,
                    sizeof(address)) == nullptr) {
        perror("inet_ntop");
        return EXIT_FAILURE;
      }
      printf("%s\t%s\n", host, address);
    }
  }
}
