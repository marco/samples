#include <algorithm>
#include <iostream>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>

using namespace std;

class non_empty_queue : public std::exception {
  std::string what_;
 public:
  explicit non_empty_queue(std::string msg) { what_ = std::move(msg); }
  [[nodiscard]] const char* what() const noexcept override  { return what_.c_str(); }
};

template<typename T>
class ThreadsafeQueue {
  std::queue<T> queue_;
  mutable std::mutex mutex_;

  // Moved out of public interface to prevent races between this
  // and pop().
  [[nodiscard]] bool empty() const {
    return queue_.empty();
  }

 public:
  ThreadsafeQueue() = default;
  ThreadsafeQueue(const ThreadsafeQueue<T> &) = delete ;
  ThreadsafeQueue& operator=(const ThreadsafeQueue<T> &) = delete ;

  ThreadsafeQueue(ThreadsafeQueue<T>&& other) noexcept(false) {
    std::lock_guard<std::mutex> lock(mutex_);
    queue_ = std::move(other.queue_);
  }

  virtual ~ThreadsafeQueue() noexcept(false) {
    std::lock_guard<std::mutex> lock(mutex_);
    if (!empty()) {
      throw non_empty_queue("Destroying a non-empty queue"s);
    }
  }

  [[nodiscard]] unsigned long size() const {
    std::lock_guard<std::mutex> lock(mutex_);
    return queue_.size();
  }

  /**
   * Retrieves the next element in the queue, if any, and
   * removes it from the queue.
   *
   * @return the next element in the queue, if any.
   */
  [[nodiscard]]
  std::optional<T> pop() {
    std::lock_guard<std::mutex> lock(mutex_);
    if (queue_.empty()) {
      return {};
    }
    T tmp = std::move(queue_.front());
    queue_.pop();
    return tmp;
  }

  /**
   * Used to "peek" the front of the queue, without removing the element;
   * it can also be used as a (threadsafe) replacement for empty, by
   * simply comparing against `nullopt`.
   *
   * Please note that, by and large, the "emptiness" property of a threadsafe
   * queue is not guaranteed to hold for long, as other threads may be
   * adding or removing items, and should not be relied upon for race-free
   * operations.
   *
   * @return a copy of the first element in the queue, if any.
   */
  [[nodiscard]]
  std::optional<T> peek() {
    std::lock_guard<std::mutex> lock(mutex_);
    if (queue_.empty()) {
      return {};
    }
    return queue_.front();
  }

  void push(T &&item) {
    std::lock_guard<std::mutex> lock(mutex_);
    queue_.push(std::forward<T>(item));
  }

  void push(const T &item) {
    std::lock_guard<std::mutex> lock(mutex_);
    queue_.push(item);
  }

  /**
   * Lexicographical comparison operator, compares the contents of
   * the two queues.
   *
   * @param rhs the other queue to compare against
   * @return the result of comparing the queues, element by element.
   */
  auto operator<=>(const ThreadsafeQueue<T>& rhs) const
  requires std::three_way_comparable<T> {
    return queue_ <=> rhs.queue_;
  }

  /**
   * Unconditionally empties the queue, throwing away all elements.
   */
  void clear() {
    std::lock_guard<std::mutex> lock(mutex_);
    while (!empty()) {
      queue_.pop();
    }
  }
};
