// Copyright (c) 2016-2020 AlertAvert.com. All rights reserved.
// Created by M. Massenzio (marco@alertavert.com)

#include <algorithm>
#include <iostream>
#include <optional>
#include <queue>
#include <thread>

#include "queue.h"

using namespace std;


void FillQueue(int from, int to, ThreadsafeQueue<int> &q) {

  auto start = std::chrono::system_clock::now();
  for (int i = from; i < to; ++i) {
    q.push(i);
    std::this_thread::sleep_for(10us);
  }
  auto runtime = std::chrono::system_clock::now() - start;
  cout << "FillQueue thread took "
       << (std::chrono::duration_cast<std::chrono::microseconds>(runtime)).count()
       << " µsec\n";
}

// NOTE: `flags` is used only by the thread running FlushQueue; does not need to be thread-safe.
std::vector<bool> flags(30, false);

void FlushQueue(ThreadsafeQueue<int> &q, int *count) {
  std::this_thread::sleep_for(100us);

  optional<int> jOpt = q.pop();
  while (jOpt) {
    int j = *jOpt;
    if (flags[j]) {
      cout << "We've already been here: " << j << endl;
      return;
    }
    flags[j] = true;
    (*count)++;
    jOpt = q.pop();
    if (!jOpt) {
      std::this_thread::sleep_for(1000us);
      jOpt = q.pop();
    }
  }
}

int main() {

  ThreadsafeQueue<int> q;
  int num_elems = 0;

  std::vector<std::thread> threads;
  threads.emplace_back(FillQueue, 0, 10, std::ref(q));
  threads.emplace_back(FillQueue, 10, 15, std::ref(q));
  threads.emplace_back(FillQueue, 15, 30, std::ref(q));

  std::thread flush(FlushQueue, std::ref(q), &num_elems);

  cout << "Threads started, waiting for them to complete...\n";
  flush.join();
  std::for_each(threads.begin(), threads.end(),
                std::mem_fn(&std::thread::join));

  cout << "We processed " << num_elems << " elements" << endl;
  cout << "After running the threads the Q has " << q.size() << " elements left" << endl;

  for (auto f : flags) {
    if (!f) {
      cout << "ERROR: we missed one\n";
    }
  }
  return EXIT_SUCCESS;
}
